# discourse-up

### Automation for my Discourse installation

To set up Discourse for installation,

1. Set up DNS so that my.example.com points to a a server accepting ssh connections

```
Warning: The following assumes you can gain access by executing:
         `ssh root@my.example.com` without entering password

# TODO: Allow specifying of ssh keys in different locations
#      Skip first section for cloud providers that provide passwordless sudo user out of the box
```

2. cd to the root of a local checkout of this repo and execute:
`./discourse-up my.example.com`

Currently this includes the installation of dependencies (note: it assumes external email service). Continue
installation with instructions from the [email section](https://github.com/discourse/discourse/blob/master/docs/INSTALL-cloud.md#email)
